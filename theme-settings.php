<?php // $Id$
// adaptivethemes.com st

/**
 * @file theme-settings.php
 */

// Include the definition of adaptivetheme_settings() and adaptivetheme_theme_get_default_settings().
include_once(drupal_get_path('theme', 'adaptivetheme') .'/theme-settings.php');

/**
* Implementation of themehook_settings() function.
*
* @param $saved_settings
*   An array of saved settings for this theme.
* @return
*   A form array.
*/
function at_panels_everywhere_settings($saved_settings) {

  // Get the default values from the .info file.
  $defaults = adaptivetheme_theme_get_default_settings('at_panels_everywhere');

  // Merge the saved variables and their default values.
  $settings = array_merge($defaults, $saved_settings);

  // Create the form using Forms API: http://api.drupal.org/api/6
  $form = array();
  // Layout settings
  $form['layout'] = array(
    '#type' => 'fieldset',
    '#title' => t('Layout settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  if ($settings['layout_enable_settings'] == 'on') {
    $image_path = drupal_get_path('theme', 'adaptivetheme') .'/css/core-images';
    $form['layout']['page_layout'] = array(
      '#type' => 'fieldset',
      '#title' => t('Page Layout'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('Use these settings to customize the layout of your theme.'),
    );
    if ($settings['layout_enable_width'] == 'on') {
      $form['layout']['page_layout']['layout_width_help'] = array(
        '#prefix' => '<div class="layout-help">',
        '#suffix' => '</div>',
        '#value' => t('<dl><dt>Page width</dt><dd>Set the overall width of the the page.</dd></dl>'),
      );
      $form['layout']['page_layout']['layout_width'] = array(
        '#type' => 'select',
        '#prefix' => '<div class="page-width">',
        '#suffix' => '</div>',
        '#default_value' => $settings['layout_width'],
        '#options' => array(
          '720px' => t('720px'),
          '780px' => t('780px'),
          '840px' => t('840px'),
          '900px' => t('900px'),
          '960px' => t('960px'),
          '1020px' => t('1020px'),
          '1080px' => t('1080px'),
          '1140px' => t('1140px'),
          '1200px' => t('1200px'),
          '1260px' => t('1260px'),
          '100%' => t('100% Fluid'),
        ),
        '#attributes' => array('class' => 'field-layout-width'),
      );
    } // endif width
  } // endif layout settings
  // Color schemes
  if ($settings['color_enable_schemes'] == 'on') {
    $form['color'] = array(
      '#type' => 'fieldset',
      '#title' => t('Color settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight' => 90,
      '#description'   => t('Use these settings to customize the colors of your site. If no stylesheet is selected the default colors will apply.'),
    );
    $form['color']['color_schemes'] = array(
      '#type' => 'select',
      '#title' => t('Color Schemes'),
      '#default_value' => $settings['color_schemes'],
      '#options' => array(
        'colors-default.css' => t('Default Color Scheme'),
      //'colors-mine.css' => t('My Color Scheme'), // add extra color css files, place these in your css/theme folder
      ),
    );
    $form['color']['color_enable_schemes'] = array(
      '#type'    => 'hidden',
      '#value'   => $settings['color_enable_schemes'],
    );
  } // endif color schemes

  // Add the base theme's settings.
  $form += adaptivetheme_settings($saved_settings, $defaults);

  // Return the form
  return $form;
}