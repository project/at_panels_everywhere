<?php // $Id$
// adaptivethemes.com

/**
 * @file template.php
 */

// Don't include custom functions if the database is inactive.
if (db_is_active()) {
  // Include base theme custom functions.
  include_once(drupal_get_path('theme', 'adaptivetheme') .'/inc/template.custom-functions.inc');
}

/**
 * Add the color scheme stylesheet if color_enable_schemes is set to 'on'.
 * Note: you must have at minimum a color-default.css stylesheet in /css/theme/
 */
if (theme_get_setting('color_enable_schemes') == 'on') {
  drupal_add_css(drupal_get_path('theme', 'at_panels_everywhere') .'/css/theme/'. get_at_colors(), 'theme');
}

/**
 * USAGE
 * 1. Rename each function to match your subthemes name,
 *    e.g. if you name your theme "themeName" then the function
 *    name will be "themeName_preprocess_hook".
 * 2. Uncomment the required function to use. You can delete the
 *    "sample_variable".
 */

/**
 * Override or insert variables into all templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered.
 */
/*
function at_panels_everywhere_preprocess(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
*/

/**
 * Override or insert variables into the page templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered.
 */
function at_panels_everywhere_preprocess_page(&$vars, $hook) {
  $classes = explode(' ', $vars['body_classes']);
  $classes = str_replace('sidebar-first', '', $classes);
  $classes = str_replace('sidebar-last', '', $classes);
  $classes = str_replace('one-sidebar', '', $classes);
  $classes = str_replace('two-sidebars', '', $classes);
  $vars['classes'] = trim(implode(' ', $classes));
}

/**
 * Override or insert variables into the node templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered.
 */
/*
function at_panels_everywhere_preprocess_node(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
*/

/**
 * Override or insert variables into the comment templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered.
 */
/*
function at_panels_everywhere_preprocess_comment(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
*/

/**
 * Override or insert variables into the block templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered.
 */
/*
function at_panels_everywhere_preprocess_block(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
*/

/**
 * Override or insert tinsel variables into the templates.
 */
function at_panels_everywhere_preprocess_pane_header(&$vars) {
  
  // set the page title var
  $vars['title'] = ctools_set_variable_token('title');
  
  // Set variables for the logo and site_name.
  if (!empty($vars['logo'])) {
    // Return the site_name even when site_name is disabled in theme settings.
    $vars['logo_alt_text'] = variable_get('site_name', '');
    $vars['linked_site_logo'] = '<a href="'. $vars['front_page'] .'" title="'. t('Home page') .'" rel="home"><img src="'. $vars['logo'] .'" alt="'. $vars['logo_alt_text'] .' '. t('logo') .'" /></a>';
  }
  if (!empty($vars['site_name'])) {
    $vars['linked_site_name'] = '<a href="'. $vars['front_page'] .'" title="'. t('Home page') .'" rel="home">'. $vars['site_name'] .'</a>';
  }

  // Hide search theme form label
  if ($vars['search_box'] && theme_get_setting('display_search_form_label') == 0) {
    $vars['toggle_label'] = ' class="hide-label"';
  }
}

/**
 * Override or insert tinsel variables into the templates.
 */
function at_panels_everywhere_preprocess_pane_navigation(&$vars) {
  // Set variables for the primary and secondary links.
  if (!empty($vars['primary_links'])) {
    $vars['primary_menu'] = theme('links', $vars['primary_links'], array('class' => 'primary-links clear-block'));
  }
  if (!empty($vars['secondary_links'])) {
    $vars['secondary_menu'] = theme('links', $vars['secondary_links'], array('class' => 'secondary-links clear-block'));
  }

  // Set variables for the primary and secondary links.
  $primary_menu_links = $vars['primary_links'];
  $secondary_menu_links = $vars['secondary_links'];

  // Output the primary and secondary menus as regular menu trees.
  if (!empty($primary_menu_links) && theme_get_setting('primary_links_tree')) {
    $primary_tree_menu = menu_tree_all_data('primary-links'); 
    $vars['primary_menu'] = menu_tree_output($primary_tree_menu);
  }
  if (!empty($secondary_menu_links) && theme_get_setting('secondary_links_tree')) {
    $secondary_tree_menu = menu_tree_all_data('secondary-links'); 
    $vars['secondary_menu'] = menu_tree_output($secondary_tree_menu);
  }
  
  $vars['breadcrumb'] = ctools_set_variable_token('breadcrumb');
}

/**
 * Override or insert tinsel variables into the templates.
 */
function at_panels_everywhere_preprocess_pane_messages(&$vars) {
  $vars['tabs'] = theme('menu_local_tasks');
  $vars['messages'] = ctools_set_variable_token('messages');
  $vars['help'] = ctools_set_variable_token('help');
}
