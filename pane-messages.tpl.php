<?php
/**
 * @file
 *
 * Theme implementation to display the messages area, which is normally
 * included roughly in the content area of a page.
 *
 * This utilizes the following variables thata re normally found in
 * page.tpl.php:
 * - $tabs
 * - $messages
 * - $help
 *
 * Additional items can be added via theme_preprocess_pane_messages(). See
 * template_preprocess_pane_messages() for examples.
 */
 ?>
<?php if ($messages or $help): ?>
  <div id="messages-and-help">
    <h2 class="element-invisible"><?php print t('System Messages'); ?></h2>
    <?php if ($messages): print $messages; endif; ?>
    <?php if ($help): print $help; endif; ?>
  </div> <!-- /messages/help -->
<?php endif; ?>
<?php if ($tabs): ?>
  <div class="local-tasks"><?php print $tabs; ?></div>
<?php endif; ?>
