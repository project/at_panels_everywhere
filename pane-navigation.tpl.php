<?php
/**
 * @file
 *
 * Theme implementation to display the messages area, which is normally
 * included roughly in the content area of a page.
 *
 * This utilizes the following variables that are normally found in
 * page.tpl.php:
 * - $primary_links
 * - $secondary_links
 * - $breadcrumb
 * - $mission
 *
 * Additional items can be added via theme_preprocess_pane_messages(). See
 * template_preprocess_pane_messages() for examples.
 */
 ?>
<?php if (!empty($primary_menu)): ?>
  <div id="primary" class="nav">
    <h2 class="element-invisible"><?php print t('Main Menu'); ?></h2>
    <?php print $primary_menu; ?>
  </div>
<?php endif; ?>
<?php if (!empty($secondary_menu)): ?>
  <div id="secondary" class="nav">
    <h2 class="element-invisible"><?php print t('Secondary Menu'); ?></h2>
    <?php print $secondary_menu; ?>
  </div>
<?php endif; ?>
<?php if ($breadcrumb): ?>
  <div id="breadcrumb">
    <h2 class="element-invisible"><?php print t('You are here:'); ?></h2>
    <?php print $breadcrumb; ?>
  </div> <!-- /breadcrumb -->
<?php endif; ?>
