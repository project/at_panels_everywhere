<?php
/**
 * @file
 *
 * Theme implementation to display the header block on a Drupal page.
 *
 * This utilizes the following variables thata re normally found in
 * page.tpl.php:
 * - $logo
 * - $front_page
 * - $site_name
 * - $front_page
 * - $site_slogan
 * - $search_box
 *
 * Additional items can be added via theme_preprocess_pane_header(). See
 * template_preprocess_pane_header() for examples.
 */
 ?>
<div id="header" class="clearfix">
  <?php if ($linked_site_logo or $linked_site_name or $site_slogan): ?>
    <div id="branding">
      <?php if ($linked_site_logo or $linked_site_name): ?>
        <?php if ($title): ?>
          <div class="logo-site-name"><strong>
            <?php if ($linked_site_logo): ?><span id="logo"><?php print $linked_site_logo; ?></span><?php endif; ?>
            <?php if ($linked_site_name): ?><span id="site-name"><?php print $linked_site_name; ?></span><?php endif; ?>
          </strong></div>
        <?php else: /* Use h1 when the content title is empty */ ?>
          <h1 class="logo-site-name">
            <?php if ($linked_site_logo): ?><span id="logo"><?php print $linked_site_logo; ?></span><?php endif; ?>
            <?php if ($linked_site_name): ?><span id="site-name"><?php print $linked_site_name; ?></span><?php endif; ?>
          </h1>
        <?php endif; ?>
      <?php endif; ?>
      <?php if ($site_slogan): ?>
        <div id="site-slogan"><?php print $site_slogan; ?></div>
      <?php endif; ?>
    </div> <!-- /branding -->
  <?php endif; ?>
  <?php if ($search_box): ?>
    <div id="search-box"<?php print $toggle_label ?>><?php print $search_box; ?></div> <!-- /search box -->
  <?php endif; ?>
</div> <!-- /header -->
